﻿using GustovCore.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GustovApi.Config
{
    public class AppOptions
    {
        public static ConnectionStrings ConnectionStrings { get; set; }
    }
}
