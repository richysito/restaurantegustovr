﻿using GustovApi.Config;
using GustovCore.Data;
using GustovCore.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gustov.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComidaController : ControllerBase
    {
        CommonDbContext db = new CommonDbContext(AppOptions.ConnectionStrings);
        public ComidaController()
        {
            db.Database.EnsureCreated();
        }

        [HttpGet("Get/{id}")]
        public Comida Get(int id)
        {
            var comida = db.Comidas.Where(p => p.Id == id).FirstOrDefault();
            return comida;
        }


        #region Get All 
        [Route("getAll")]
        [HttpGet]
        public IEnumerable<Comida> GetAll()
        {
            var lista = db.Set<Comida>();
            return lista;
        }
        #endregion

        #region Insert 
        
        [HttpPost]
        public void Post([FromBody] Comida entidad)
        {
            var result = db.Comidas.Add(entidad);
            db.SaveChanges();
        }
        #endregion

        #region Update
        [HttpPut]
        public void Put([FromBody] Comida entidad)
        {
            db.Comidas.Update(entidad);
            db.SaveChanges();
        }
        #endregion

        #region Delete      
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var entity = db.Comidas.Where(p => p.Id == id).FirstOrDefault();
            db.Comidas.Remove(entity);
            //  await _query.Delete(id);
            db.SaveChanges();
        }
        #endregion
    }
}
