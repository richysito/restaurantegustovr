﻿using AspNetCore.Reporting;
using GustovApi.Config;
using GustovCore.Data;
using GustovCore.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        CommonDbContext db = new CommonDbContext(AppOptions.ConnectionStrings);
        public ReportController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            db.Database.EnsureCreated();
        }

        [Route("getReport")]
        [HttpGet]
        public List<VentaRep> GetReport(DateTime date)
        {
            var listventas = listaVenta(date);
            return listventas;
        }
        public List<VentaRep> listaVenta(DateTime date)
        {
            var lista = db.Set<Venta>().Include(p => p.Comida).Where(x => x.Fecha.Year == date.Year && x.Fecha.Month == date.Month && x.Fecha.Day == date.Day);
            var listventas = new List<VentaRep>();
            foreach (var item in lista)
            {
                var entidad = new VentaRep();
                entidad.NombreCliente = item.NombreCliente;
                entidad.NombreComida = item.Comida.Nombre;
                entidad.Cantidad = item.Cantidad;
                entidad.Precio = item.Precio;
                entidad.Total = (item.Cantidad * item.Precio);
                listventas.Add(entidad);
            }
            return listventas;
        }


        [HttpGet]
        [Route("Export_Data")]
        public ActionResult Export_Data(DateTime date)
        {

            var byteRes = new byte[] { };
            string path = _hostingEnvironment.ContentRootPath + "\\Report\\VentaReport.rdlc";

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            LocalReport report = new LocalReport(path);
            List<VentaRep> l = new List<VentaRep>();
             l = listaVenta(date);

            report.AddDataSource("DataSet1", l);
            var result = report.Execute(RenderType.Pdf, 1);
            byteRes = result.MainStream;


            return File(byteRes,
                System.Net.Mime.MediaTypeNames.Application.Octet,
                "ReportName.pdf");
        }
    }
}
