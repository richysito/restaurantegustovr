﻿using GustovApi.Config;
using GustovCore.Data;
using GustovCore.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GustovApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class VentaController : ControllerBase
    {
        CommonDbContext db = new CommonDbContext(AppOptions.ConnectionStrings);
        public VentaController()
        {
            db.Database.EnsureCreated();
        }

        [HttpGet("Get/{id}")]
        public Venta GetVenta(int id)
        {
            var venta = db.Set<Venta>().Include(p => p.Comida).Where(p => p.Id == id).FirstOrDefault();
            return venta;
        }

        #region Get All 
        [Route("getAll")]
        [HttpGet]
        public IEnumerable<Venta> GetAll()
        {
            var lista = db.Set<Venta>().Include(p => p.Comida);
            return lista;
        }
        #endregion

        #region Insert 
        [HttpPost]
        public void Post([FromBody] Venta entidad)
        {
            entidad.ComidaId = entidad.Comida.Id;
            entidad.Comida = null;
            entidad.Fecha = DateTime.Now;
            var result = db.Ventas.Add(entidad);
            db.SaveChanges();
        }
        #endregion

        #region Update
        [HttpPut]
        public void Put([FromBody] Venta entidad)
        {
            entidad.Comida = null;
            db.Ventas.Update(entidad);
            db.SaveChanges();
        }
        #endregion

        #region Delete 
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            var entity = db.Ventas.Where(p => p.Id == id).FirstOrDefault();
            db.Ventas.Remove(entity);
            //  await _query.Delete(id);
        }
        #endregion
    }
}
