﻿using GustovCore.Base;
using GustovCore.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovCore.Data
{
    public class ComidaConfiguration : EntityTypeConfiguration<Comida>
    {
        public override void Map(EntityTypeBuilder<Comida> builder)
        {
            builder.ToTable("Comida");
            builder.Property(e => e.Id).HasColumnName("ComidaId");
        }
    }
}
