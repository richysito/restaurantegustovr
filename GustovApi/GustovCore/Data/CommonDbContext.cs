﻿using GustovCore.Base;
using GustovCore.Config;
using GustovCore.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovCore.Data
{
    public class CommonDbContext : DbContext
    {
        private string _connectionString = "ConnectionString";
        public CommonDbContext()
        {
        }

        public CommonDbContext(ConnectionStrings connectionStrings)
        {
            _connectionString = connectionStrings.DefaultConnection;
        }
        public DbSet<Comida> Comidas { get; set; }
        public DbSet<Venta> Ventas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.AddConfiguration(new ComidaConfiguration());
            modelBuilder.AddConfiguration(new VentaConfiguration());
        }
    }
}

