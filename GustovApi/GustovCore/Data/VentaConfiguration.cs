﻿using GustovCore.Base;
using GustovCore.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovCore.Data
{
    class VentaConfiguration : EntityTypeConfiguration<Venta>   
    {
        public override void Map(EntityTypeBuilder<Venta> builder)
        {
            builder.ToTable("Venta");
            builder.Property(e => e.Id).HasColumnName("VentaId");
        }
    }
}
