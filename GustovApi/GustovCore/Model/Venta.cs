﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovCore.Model
{
    public class Venta
    {
        public int Id { get; set; }
        public string NombreCliente { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public DateTime Fecha { get; set; }
        public int ComidaId { get; set; }
        //[NotMapped]
        public virtual Comida Comida { get; set; }
    }
}
