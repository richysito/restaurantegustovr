﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovCore.Model
{
    public class VentaRep
    {

        public string NombreCliente { get; set; }
        public string NombreComida { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Total { get; set; }

    }

}
