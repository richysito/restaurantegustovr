﻿using GustovApi;
using GustovCore.Model;
using GustovTest.Maqueta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GustovTest
{
    public class ComidaTest : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public ComidaTest(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }
        #region Get Tes

        [Fact]
        public async Task GetTest()
        {
            var entidad = MaquetaComida.GuardarMaqueta(1).FirstOrDefault();
            var request = "Api/Comida/Get/"+ entidad.Id;
            var response = await Client.GetAsync(request);
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Get All Test

        [Fact]
        public async Task GetAll()
        {
            var entidades = MaquetaComida.GuardarMaqueta(5);
            var request = "Api/Comida/GetAll";
            var response = await Client.GetAsync(request);

            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Insert Test

        [Fact]
        public async Task InsertTest()
        {
           
            var request = new
            {
               Url = "Api/Comida",
               Body = MaquetaComida.CrearMaqueta(1).FirstOrDefault()
            };

            var response = await Client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Update Test
        [Fact]
        public async Task UpdateTest()
        {

            var entidad = MaquetaComida.GuardarMaqueta(1).FirstOrDefault();
            entidad.Nombre = "nombreUpdate" + Guid.NewGuid().ToString(); 
            var request = new
            {
                Url = "Api/Comida",
                Body = entidad
            };

            // Act
            var response = await Client.PutAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Delete Test

        [Fact]
        public async Task DeleteTest()
        {
            var entity = await Task.Run(async () =>
            {
                var entidad = MaquetaComida.GuardarMaqueta(1);
                return entidad;
            }).ContinueWith(async (entidad) =>
            {
                var id = entidad.Result.FirstOrDefault().Id;
                var request = "Api/Comida/" + id;
                var response = await Client.DeleteAsync(request);

                response.EnsureSuccessStatusCode();
            });

        }
        #endregion

    }
}
