﻿using GustovApi.Config;
using GustovCore.Data;
using GustovCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovTest.Maqueta
{
    public static class MaquetaComida
    {
        public static CommonDbContext db = new CommonDbContext(AppOptions.ConnectionStrings);
        #region Crear Maqueta

        public static List<Comida> CrearMaqueta(int numMaquetas)
        {
            var maquetas = new List<Comida>();

            for (var index = 1; index <= numMaquetas; index++)
            {
                var maqueta = new Comida
                {
                    Nombre = "nombreTest" + Guid.NewGuid().ToString(),
                    Precio = 12,

                };
                maquetas.Add(maqueta);
            }
            return maquetas;
        }
        #endregion

        #region Crear y Guardar maqueta

        public static List<Comida> GuardarMaqueta(int numMaquetas)
        {
            var maquetas = CrearMaqueta(numMaquetas);
            foreach (var item in maquetas)
            {
                db.Comidas.Add(item);
                db.SaveChanges();
            }
            return maquetas;
        }
        #endregion
    }
}
