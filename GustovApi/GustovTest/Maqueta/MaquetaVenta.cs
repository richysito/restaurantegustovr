﻿using GustovApi.Config;
using GustovCore.Data;
using GustovCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GustovTest.Maqueta
{
    public static class MaquetaVenta
    {
        public static CommonDbContext db = new CommonDbContext(AppOptions.ConnectionStrings);
        #region Crear Maqueta

        public static List<Venta> CrearMaqueta(int numMaquetas)
        {
            var maquetas = new List<Venta>();
            var comida = MaquetaComida.GuardarMaqueta(1).FirstOrDefault();

            for (var index = 1; index <= numMaquetas; index++)
            {
                var maqueta = new Venta
                {
                    NombreCliente = String.Format( "Cliente {0}", Guid.NewGuid().ToString().Substring(0,10)),
                    Precio = comida.Precio,
                    Comida = comida,
                    Cantidad = 2,
                    Fecha = DateTime.Now,
                    ComidaId = comida.Id

                };
                maquetas.Add(maqueta);
            }
            return maquetas;
        }
        #endregion

        #region Crear y Guardar maqueta

        public static List<Venta> GuardarMaqueta(int numMaquetas)
        {
            var maquetas = CrearMaqueta(numMaquetas);
            foreach (var item in maquetas)
            {
                item.ComidaId = item.Comida.Id;
                item.Comida = null;
                db.Ventas.Add(item);
                db.SaveChanges();
            }
            return maquetas;
        }
        #endregion
    }
}
