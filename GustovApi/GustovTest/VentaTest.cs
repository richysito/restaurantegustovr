﻿using GustovApi;
using GustovCore.Model;
using GustovTest.Maqueta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GustovTest
{
    public class VentaTest : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public VentaTest(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }
        #region Get Tes

        [Fact]
        public async Task GetTest()
        {
            var entidad = MaquetaVenta.GuardarMaqueta(1).FirstOrDefault();
            var request = "Api/Venta/Get/" + entidad.Id;
            var response = await Client.GetAsync(request);
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Get All Test

        [Fact]
        public async Task GetAll()
        {
            var request = "Api/Venta/GetAll";
            var response = await Client.GetAsync(request);

            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Insert Test

        [Fact]
        public async Task InsertTest()
        {
           
          
            var request = new
            {
                Url = "Api/Venta",
                Body = MaquetaVenta.CrearMaqueta(1).FirstOrDefault()
            };

            var response = await Client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Update Test
        [Fact]
        public async Task UpdateTest()
        {
            var entidad = MaquetaVenta.GuardarMaqueta(1).FirstOrDefault();
            entidad.NombreCliente = "nombreUpdate" + Guid.NewGuid().ToString();
            var request = new
            {
                Url = "Api/Venta    ",
                Body = entidad
            };

            // Act
            var response = await Client.PutAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            var value = await response.Content.ReadAsStringAsync();

            // Assert
            response.EnsureSuccessStatusCode();
        }

        #endregion

        #region Delete Test


        #endregion

       

    }
}
