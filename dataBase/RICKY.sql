USE [Gustov]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 9/3/2021 12:25:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[ClienteId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](150) NULL,
	[NitCi] [nvarchar](20) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[ClienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comida]    Script Date: 9/3/2021 12:25:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comida](
	[ComidaId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Precio] [money] NOT NULL,
 CONSTRAINT [PK_Comida] PRIMARY KEY CLUSTERED 
(
	[ComidaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 9/3/2021 12:25:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[VentaId] [int] IDENTITY(1,1) NOT NULL,
	[ComidaId] [int] NOT NULL,
	[NombreCliente] [nvarchar](50) NULL,
	[Precio] [money] NULL,
	[Cantidad] [int] NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[VentaId] ASC,
	[ComidaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Comida] ON 

INSERT [dbo].[Comida] ([ComidaId], [Nombre], [Precio]) VALUES (1, N'Picante de pollo', 12.0000)
INSERT [dbo].[Comida] ([ComidaId], [Nombre], [Precio]) VALUES (2, N'Charque', 30.0000)
INSERT [dbo].[Comida] ([ComidaId], [Nombre], [Precio]) VALUES (3, N'Pique', 25.0000)
INSERT [dbo].[Comida] ([ComidaId], [Nombre], [Precio]) VALUES (4, N'Pailita', 15.0000)
SET IDENTITY_INSERT [dbo].[Comida] OFF
GO
SET IDENTITY_INSERT [dbo].[Venta] ON 

INSERT [dbo].[Venta] ([VentaId], [ComidaId], [NombreCliente], [Precio], [Cantidad], [Fecha]) VALUES (1, 1, N'Sin Nombre', 12.0000, 3, CAST(N'2021-09-03T12:21:38.280' AS DateTime))
SET IDENTITY_INSERT [dbo].[Venta] OFF
GO
