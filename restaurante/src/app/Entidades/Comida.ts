export class Comida {
  id : number;
  nombre : String;
  precio : number;

  constructor(obj: Comida){
    this.id = obj.id;
    this.nombre = obj.nombre;
    this.precio = obj.precio;
  }

}
