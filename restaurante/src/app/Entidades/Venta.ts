import { Comida } from "./Comida";

export class Venta {
  ventaId : number;
  comida : Comida;
  nombreCliente : string;
  precio : number;
  cantidad : number;
  fecha : Date;


  constructor(obj: Venta){
    this.ventaId = obj.ventaId;
    this.comida = obj.comida;
    this.nombreCliente = obj.nombreCliente;
    this.precio = obj.precio;
    this.cantidad = obj.cantidad;
    this.fecha = obj.fecha;
  }



}
