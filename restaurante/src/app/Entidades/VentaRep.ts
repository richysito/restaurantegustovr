import { Comida } from "./Comida";

export class VentaRep {
  nombreCliente : string;
  nombreComida : Comida;
  cantidad : number;
  precio : number;
  fecha : Date;
  total : number;


  constructor(obj: VentaRep){
    this.nombreCliente = obj.nombreCliente;
    this.nombreComida = obj.nombreComida;
    this.precio = obj.precio;
    this.cantidad = obj.cantidad;
    this.fecha = obj.fecha;
    this.total = obj.total;
  }



}
