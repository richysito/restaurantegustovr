import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Comida } from "../Entidades/Comida";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ComidaService {


  constructor(private http: HttpClient){

  }
  agregar(comida :Comida) : Observable<any>{
    if(comida.id){
      return   this.http.put("/api/Comida/", comida);
    }else{
      return   this.http.post("/api/Comida/", comida);
    }


  }
  editar(id:number){

  }
  eliminar(id:number){
    return   this.http.delete("/api/Comida/"+ id);
  }

  getAll() : Observable<Array<Comida>>{
    return   this.http.get<Array<Comida>>("/api/Comida/getAll");

    }
}
