import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Venta } from "../Entidades/Venta";

@Injectable({
  providedIn: "root"
})
export class VentaService {


  constructor(private http: HttpClient){

  }
  agregar(venta :Venta) : Observable<any>{
    if(venta.ventaId){
      return   this.http.put("/api/Venta/", venta);
    }else{
      return   this.http.post("/api/Venta/", venta);
    }


  }
}
