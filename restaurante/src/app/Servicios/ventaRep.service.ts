import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { VentaRep } from "../Entidades/VentaRep";

@Injectable({
  providedIn: "root"
})
export class VentaRepService {


  constructor(private http: HttpClient){

  }
  getByDate(date:Date) : Observable<Array<VentaRep>>{
    const params :HttpParams =new HttpParams().append('date', date.toString());
    return   this.http.get<Array<VentaRep>>("/api/report/getreport/", {params:params});

    }
    downloadReport(date:Date) : Observable<any>{
      return   this.http.get<any>("/api/report/Export_Data/"+ date);

      }

}



