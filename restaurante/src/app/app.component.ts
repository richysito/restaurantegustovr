import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Restaurante';
  fullImagePath: string;
  constructor(private router: Router){
    this.fullImagePath = '../../restaurant.jpg'
  }

  redirectComida(){
    this.router.navigateByUrl('/app-comidas/');
  }

}
