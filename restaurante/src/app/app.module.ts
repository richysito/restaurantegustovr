import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ComidasComponent } from './components/comidas/comidas.component';
import { ReportesComponent } from './components/reportes/reportes.component';
import { MenuComponent } from './components/menu/menu.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { VentasComponent } from './ventas/ventas.component';


const routes: Routes = [

  { path: 'app-comidas', component: ComidasComponent },
  { path: 'app-ventas', component: VentasComponent },
  { path: 'app-reportes', component: ReportesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ComidasComponent,
    ReportesComponent,
    MenuComponent,
    VentasComponent
  ],

  imports: [
    BrowserModule, RouterModule.forRoot(routes), HttpClientModule, FormsModule,
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
