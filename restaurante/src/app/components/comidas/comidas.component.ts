import { Component, OnInit } from '@angular/core';
import { Comida } from 'src/app/Entidades/Comida';
import { ComidaService } from 'src/app/Servicios/comida.service';

@Component({
  selector: 'app-comidas',
  templateUrl: './comidas.component.html',
  styleUrls: ['./comidas.component.css']
})
export class ComidasComponent implements OnInit {
  comidaList: Comida[];
  comida : Comida;

  constructor(public comida$: ComidaService) {
    this.comidaList = [];
    this.comida = new Comida({id :0, nombre: "", precio:0});
  }

  ngOnInit(): void {
   this.getAll();
  }
  agregar(){
    this.comida = new Comida({id :0, nombre: "", precio:0});
  }
  editar(item :Comida){
    this.comida = item;
  }
  eliminar(id:number){
    this.comida$.eliminar(id).subscribe(p=>{
      alert("comida eliminada");
      this.getAll();
    })
  }
  getAll(){
    this.comida$.getAll().subscribe(p=>{
      this.comidaList = p;
    })

  }
  guardar(){
    this.comida$.agregar(this.comida).subscribe(p=>{
      alert("comida ingresada");
      this.getAll();
    })
  }



}
