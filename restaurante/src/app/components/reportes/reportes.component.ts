import { Component, OnInit } from '@angular/core';
import { VentaRep } from 'src/app/Entidades/VentaRep';
import { VentaRepService } from 'src/app/Servicios/ventaRep.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  reportLista : Array<VentaRep>;
  dateSearch : Date;
  constructor(public ventaRep$ : VentaRepService) {
    this.reportLista = [];
    this.dateSearch = new Date();
   }

  ngOnInit(): void {
  }

  getByDate(){
    this.ventaRep$.getByDate(this.dateSearch).subscribe(p=>{
      this.reportLista = p;
    })

  }
  downloadReport(){
    this.ventaRep$.downloadReport(this.dateSearch).subscribe(()=>{
      alert("Reporte Descargado");
    })

  }


}
