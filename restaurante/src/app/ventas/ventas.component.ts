import { Component, OnInit } from '@angular/core';
import { Comida } from '../Entidades/Comida';
import { Venta } from '../Entidades/Venta';
import { ComidaService } from '../Servicios/comida.service';
import { VentaService } from '../Servicios/venta.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  dateNow : Date
  venta : Venta
  comidaList : Array<Comida>;

  constructor(public comida$: ComidaService, public venta$:VentaService) {
    this.dateNow = new Date();
    this.venta = new Venta({ventaId: 0, comida:new Comida({id:0,nombre:" ", precio:0}) , nombreCliente:" ", precio:0, cantidad: 0, fecha:new Date()});
    this.comidaList =[];
   }

  ngOnInit(): void {
    this.getAll();
  }
  getAll(){
    this.comida$.getAll().subscribe(p=>{
      this.comidaList = p;
    })

  }

  calcularPrecio(){
    return  this.venta.comida.precio * this.venta.cantidad;

  }

  guardarVenta(){
    this.venta.precio = this.calcularPrecio();
    this.venta$.agregar(this.venta).subscribe(()=>{
      alert("Venta Guardada");
      this.venta = new Venta({ventaId: 0, comida:new Comida({id:0, nombre:" ", precio:0}) , nombreCliente:" ", precio:0, cantidad: 0, fecha:new Date()});
    })
  }

}
